\contentsline {section}{\numberline {1.1}Introduction and background}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}The frozen spin method}{1}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}The simulation}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Equations of motion derivation}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Proper time and decay}{3}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}The E and B fields}{3}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Ramping}{4}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Final equations}{5}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Implementation}{5}{section.1.3}
\contentsline {subsubsection}{Structure overview}{6}{section*.7}
\contentsline {subsubsection}{The solver's \emph {events} system}{6}{section*.8}
\contentsline {section}{\numberline {1.4}Results}{7}{section.1.4}
