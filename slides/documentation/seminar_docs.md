---
title: Documentation for Spatial-Temporal Correlated distribution generator
author: Francesco Forcher
date: June 30, 2019
geometry: margin=2cm
output: pdf_document
---

## Introduction
We want to generate a distribution with a correlation between time of emission
and spatial location of the particle. We want to generate a text file to be used
as a `FROMFILE` distribution with attribute `EMITTED=TRUE` for OPAL (see table 9 [here](https://gitlab.psi.ch/OPAL/Manual-2.1/wikis/distribution#sec.distribution.fromfiledisttype)).


## The idea
The physical realization involves a laser bunch which passes trough a prism, to create a tilted front
(see image \ref{prism}).
![Tilted front\label{prism}](./img/prism2.png)

We will use an elliptic-shaped moving "stencil" that creates the desired distribution.
More concretely, we first establish the start and stop.
