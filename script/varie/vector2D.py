import matplotlib.pyplot as plt
import numpy as np

# X = np.arange(-10, 10, 1)
# Y = np.arange(-10, 10, 1)
# U, V = np.meshgrid(X, Y)

# def sextupole_section(x, y, K, J=0):
#     mylogg = logg.getLogger('mylog_partclass')
#     Bx = J*(x**2 - y**2)+K*y*x
#     By = -J*x*y + K*(x**2 - y**2)
#     mylogg.debug(f"|Bsext| = {np.sqrt(Bx**2+By**2)}")
#     return np.array([Bx,By])

X, Y = np.meshgrid(np.arange(-1, 1, .1), np.arange(-1, 1, .1))
J=0
K=1
Bx = J*(X**2 - Y**2)+K*Y*X
By = -J*X*Y + K*(X**2 - Y**2)



fig1, ax1 = plt.subplots()
ax1.set_title('Arrows scale with plot width, not view')
Q = ax1.quiver(X, Y, Bx, By, units='width')
qk = ax1.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E',
                   coordinates='figure')

plt.show()

fig2, ax2 = plt.subplots()
ax2.set_title('Stream')
S = ax1.streamplot(X, Y, Bx, By)
# qk = ax1.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E',
#                    coordinates='figure')

plt.show()