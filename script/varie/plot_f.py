import matplotlib.pyplot as plt
import numpy as np

# 100 linearly spaced numbers
t = np.linspace(0,1,100) 

# the fu(nction, which is y = x^2 here
def ramp_down(t, start_r=1, stop_r=2):
    y1 = 1
    y2 = 0
    x1 = start_r
    x2 = stop_r
    m = (y2-y1) / (x2-x1)
    if t <= start_r:
        return 1
    elif t > start_r and t <= stop_r:
        return m*(t-x1)+y1
    else:
        return 0
    pass
    
y = [ramp_down(tt,0.5,0.7) for tt in t]

# setting the axes at the centre
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
# ax.spines['left'].set_position('center')
# ax.spines['bottom'].set_position('zero')
# ax.spines['right'].set_color('none')
# ax.spines['top'].set_color('none')
# ax.xaxis.set_ticks_position('bottom')
# ax.yaxis.set_ticks_position('left')



# plot the function
plt.plot(t,y, 'r') 

plt.title('''Ramping factor $a(t)$''')
#plt.ylabel('''$r-r_0$ [cm]''')
plt.xlabel('Time [μsec]')

# show the plot
plt.show()
