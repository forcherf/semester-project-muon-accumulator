import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
#from .mylib.settings import GlobalParameters
from ai import cs # Coordinate transform library


import matplotlib.pyplot as plt
#plt.rc('text', usetex=True)
img_type = "pdf"


xy = np.array([
    [-9,0.],
    [-8., 80.],
    [-7., 124.], 
    [-5.,140.], 
    [-3,100],
    [-2,40],
    [-1,18],
    [0,10],

    [1,5],
    [3,-80],
    #[4,-115],
    [5.,-120.],
    [7.,-108.],
    [8.,-60.],
    [9.,0.]
    ])

#xy = GlobalParameters.Bz_pert_datapoints

x = xy[:,0]
y = xy[:,1]

f2 = scipy.interpolate.interp1d(x, y, kind=5, bounds_error=False, fill_value=0.)

xp = np.arange(-10, 10, 0.2)
yp = f2(xp)

plt.plot(xp,yp,label='Spline')
plt.plot(x, y, marker='o', linestyle='',
    linewidth=2, markersize=6,label='Points')

plt.title('''Interpolation of $Bz_p(r)$''')
plt.xlabel('''$r-r_0$ [cm]''')
plt.ylabel('$Bz_p(r-r0)$ [Gauss]')
plt.legend(loc='best')
plt.savefig("../plots/Bz_interpolation."+img_type)
plt.show()



################
################ PLOT 3D field
fig = plt.figure()
ax = fig.gca(projection='3d')

# Make the grid
xc = np.arange(-0.8, 0.8, 0.02)
yc = np.arange(-0.8, 0.8, 0.02)
zc = np.array([0.])
x, y, z = np.meshgrid(xc,
                      yc,
                      zc, sparse=False, indexing='xy')
print(z)
print(z[0,1,0])
print(np.shape(x))
print(np.shape(z))
nx = len(xc)
ny = len(xc)
nz = len(zc)

####
#### This...
####
# Make the direction data for the arrows
# u = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
# v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
# w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
#      np.sin(np.pi * z))
####
#### ...is equivalent to:
####
# u = np.zeros(shape=(nx,ny,nz))
# v = np.zeros(shape=(nx,ny,nz))
# w = np.zeros(shape=(nx,ny,nz))
# for i in range(nx):
#     for j in range(ny):
#         for k in range(nz):
#             u[i,j,k] = np.sin(np.pi * x[i,j,k]) * np.cos(np.pi * y[i,j,k]) * np.cos(np.pi * z[i,j,k])
#             v[i,j,k] = -np.cos(np.pi * x[i,j,k]) * np.sin(np.pi * y[i,j,k]) * np.cos(np.pi * z[i,j,k])
#             w[i,j,k] = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x[i,j,k]) * np.cos(np.pi * y[i,j,k]) *
#                  np.sin(np.pi * z[i,j,k]))
Bz_pert_scaleB = 1.
rmr0 = xy[:,0]/100  # [m]
Bz_p = xy[:,1]/10e4*Bz_pert_scaleB # [Tesla]
f_Bzsec = scipy.interpolate.interp1d(rmr0, Bz_p, kind=5, bounds_error=False, fill_value=0.)

def B_perturbator(x, y, z, t):
    pi = np.pi

    # phi = 0 is the positive x axis, pos y = pi/2, neg x pi, neg y MINUS pi/2
    r, phi, z = cs.cart2cyl(x=x, y=y, z=z)
    p_ang_width = 40*pi/180. # 40 degrees
    p_end = -pi/2# |
    p_start=-pi/2-p_ang_width # /

    r0 = 0.42
    xl = r-r0
    x1 = r0-0.10
    x2 = r0+0.10
    if phi > p_start and phi < p_end and xl+r0 > x1 and xl+r0 < x2:
        #print('here')
        #Bz = 0.011*2/(x2-x1)*xl
        Bz = f_Bzsec(xl)
    else:
        Bz = 0.
    return np.array([0.,0.,Bz])

r0=0.42
u = np.zeros(shape=(nx,ny,nz))
v = np.zeros(shape=(nx,ny,nz))
wpos = np.zeros(shape=(nx,ny,nz))
wneg = np.zeros(shape=(nx,ny,nz))
for i in range(nx):
    for j in range(ny):
        for k in range(nz):
            #print(f"{i}, {j}, {k}")
            xx = x[i,j,k]
            yy = y[i,j,k]
            zz = 0.
            # r, phi, z = cs.cart2cyl(x=xx, y=yy, z=0.)
            # #print(r-r0)
            # print(f2(r-r0))
            # u[i,j,k] = 0.
            # v[i,j,k] = 0.
            # w[i,j,k] = f2(r-r0)
            Bz = B_perturbator(xx, yy, zz, t=0.)[2]
            u[i,j,k] = 0.
            v[i,j,k] = 0.
            if Bz >= 0:
                wpos[i,j,k] = Bz
                wneg[i,j,k] = 0
                pass
            else:
                wpos[i,j,k] = 0
                wneg[i,j,k] = Bz
                pass
            

plt.title(r'''${B_p}(r)$''')
ax.quiver(x, y, z, u, v, wpos, length=20, arrow_length_ratio=0.2, normalize=False, color='g')
ax.quiver(x, y, z, u, v, wneg, length=20, arrow_length_ratio=0.2, normalize=False, color='r')
ax.set_xlabel('X [m]')
ax.set_ylabel('Y [m]')
ax.set_zlabel('Z [m]')

plt.savefig("../plots/3D_perturbator_field."+img_type)
plt.show()
