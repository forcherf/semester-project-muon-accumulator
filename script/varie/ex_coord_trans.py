import numpy as np
from ai import cs

# cartesian to spherical
r, theta, phi = cs.cart2sp(x=1, y=1, z=1)

# spherical to cartesian
x, y, z = cs.sp2cart(r=1, theta=np.pi/4, phi=np.pi/4)

# cartesian to cylindrical
r, phi, z = cs.cart2cyl(x=1, y=1, z=1)

# cylindrical to cartesian
x, y, z = cs.cyl2cart(r=1, phi=np.pi/2, z=1)
