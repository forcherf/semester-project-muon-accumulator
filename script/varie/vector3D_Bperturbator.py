# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np
from ai import cs

def B_perturbator(x, y, z):
    pi = np.pi

    # phi = 0 is the positive x axis, pos y = pi/2, neg x pi, neg y MINUS pi/2
    r, phi, z = cs.cart2cyl(x=x, y=y, z=z)
    p_ang_width = 40*pi/180. # 40 degrees
    p_end = -pi/2# |
    p_start=-pi/2-p_ang_width # /

    r0 = 0.42
    xl = r-r0
    x1 = r0-0.05
    x2 = r0+0.05
    if phi > p_start and phi < p_end and xl+r0 > x1 and xl+r0 < x2:
        #print('here')
        Bz = 0.011*2/(x2-x1)*xl
    else:
        Bz = 0
    return np.array([0,0,Bz])


fig = plt.figure()
ax = fig.gca(projection='3d')

# Make the grid
# x, y, z = np.meshgrid(np.arange(-0.8, 1, 0.2),
#                       np.arange(-0.8, 1, 0.2),
#                       np.arange(-0.8, 1, 0.8))
x, y, z = np.meshgrid(np.arange(-0.5, 0.5, 0.05),
                      np.arange(-0.5, 0.5, 0.05),
                      0)





# Make the direction data for the arrows
# u=np.zeros(len(x))
# v=np.zeros(len(x))
# w=np.zeros(len(x))
for xi in range(len(x)):
    for yi in range(len(y)):
        # u[] = 0
        # v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
        # w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
        #     np.sin(np.pi * z))
u = 0
v = 0
w = B_perturbator(x, y, z)[2]


ax.quiver(x, y, z, u, v, w, length=0.1, normalize=True)

plt.show()

