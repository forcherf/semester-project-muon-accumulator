import numpy as np
#import matplotlib as mpl
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#import sympy as syp
from ai import cs # Coordinate transform library

from scipy.integrate import solve_ivp
import logging as logg
import argparse
import time
import os

from mylib.physics import Constants,getgamma,getbeta
from mylib.settings import GlobalParameters
from mylib import particles_classes, utils
from mylib.simulation_functions import sim_one_particle,sim_bunch

def plot_one_particle(decay_time_pt=None):
    mylogg = logg.getLogger('mylog')
    ### Solve
    mylogg.info("Solve equations of motion...")
    sol, part_stat = sim_one_particle(#particle_id=0, 
                        particle_class=particles_classes.MuonEMClass(), 
                        Nvals=5000, 
                        beta0=0.77,
                        start_time = 0,
                        max_lifetime=None,
                        safety_margin_factor = 1.2,
                        keep_every = 1,
                        decay_time_pt=None,
                        )
    ###

    mylogg.info("Plot...")
    ### Plot 3D
    utils.plotsol_3D(sol)
    ###

    ### Plot 2D
    utils.plotsol_2D(sol)
    ###

    utils.plotsol_r_minus_r0(sol)
    pass

def main():
    ### Declare command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--log", dest='loglevel',help="Log level, not case sentitive, in order of severity: \n" 
                                                      "  DEBUG INFO WARNING ERROR CRITICAL. \n"
                                                      "  Default: WARNING.")
    parser.add_argument("--proc-id", dest='process_ID', default=1, type=int,
                        help="Local process ID, used to tag muons in the generated files.\n"
                             "  Should be different for each process if doing a parallel run\n" 
                             "  Default: 1.")
    parser.add_argument("--num-part", dest='num_part', default=1, type=int,
                        help = "Number of particles to simulate for this process \n"
                               "  Default: 1.")
    parser.add_argument("--run-id", dest='run_id', default=-1, type=int,
                    help = "The run ID used to make the folders. If not set it generates a new id \n"
                            "  Default: -1.")
    args = parser.parse_args()
    ###

    datafile = utils.generate_unique_filename(".h5")
    logfile = utils.generate_unique_filename(".log")

    test_mydatadir = f"./data/runM{args.run_id}"
    test_mylogdir = f"./log/runM{args.run_id}"
    if os.path.exists(test_mydatadir) or os.path.exists(test_mylogdir):
        raise ValueError(f"Error: Run folders with ID M{args.run_id} exist already\n"
                         f"Try another rID value for --run-id=rID ")
    datadir = utils.generate_run_folder("data", runID = args.run_id)
    logdir = utils.generate_run_folder("log", runID = args.run_id)


    ### Set log level
    
    numeric_level = getattr(logg, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    mylogg = logg.getLogger('mylog') # __name__='__main__'?
    mylogg.setLevel(level=numeric_level)
    logg.basicConfig(filename=f"{logdir}/{logfile}", level=numeric_level)

    logg.getLogger("matplotlib").setLevel(logg.WARNING) #Disable plt debug stuff

    ###

    mylogg.info("Start program")
    startpt_tot = time.perf_counter()
    startproct_tot = time.process_time()
    
    mylogg.info(__name__)
    


    #plot_one_particle(particles_classes.MuonEMClass.hl_rf*1.57/15)
    #plot_one_particle(particles_classes.MuonEMClass.hl_rf)
    
    
    init_cond = utils.make_square_initcond(beta0=0.77, every=3)
    avg_gamma = 1.57
    time_cutoff_lab = 5e-6
    sim_bunch(  datadir = datadir,
                filename = datafile, 
                N=args.num_part, 
                pID=args.process_ID, 
                initial_distr= init_cond,
                decay_time_pt = time_cutoff_lab / avg_gamma)
    #utils.plotsol_rphase(run_id='M2', n_particles=len(init_cond))





    endpt_tot = time.perf_counter()
    endproct_tot = time.process_time()
    totpt = endpt_tot - startpt_tot
    totproct = endproct_tot - startproct_tot
    mylogg.info(f"Processing Done")
    mylogg.info(f"Process runtime: {totproct} [s]")
    mylogg.info(f"Total system runtime: {totpt} [s]")
    mylogg.info("End program")
    

    pass



if __name__ == "__main__":
    main()
