import logging as logg
import numpy as np

class GlobalParameters:
    default_log_level = logg.WARNING
    plot_file_type='pdf'
    plot_show = True
    
    accelerator_height = 1
    decay_percentile_to_ignore = 0.03

    #ramp_start_time = 6.9e-7
    ramp_start_time = 5e-7
    #ramp_start_time = 0
    ramp_end_time = 6e-7

    gyrorad_theory = 0.426509928278128 # With Bz=1 [T] and Er=0.63e6 [V/m]

    Bz_pert_scaleB = 1
    Bz_pert_datapoints = np.array([ # x: cm from r0, y: Bz [Gauss]
        [-9,0.],
        [-8., 80.],
        [-7., 124.], 
        [-5.,140.], 
        [-3,100],
        [-2,40],
        [-1,18],
        [0,10],

        [1,5],
        [3,-80],
        #[4,-115],
        [5.,-120.],
        [7.,-108.],
        [8.,-60.],
        [9.,0.]
    ])