import numpy as np
from .physics import Constants as cst

class Gaussian():
    
    # @param mu specifies mean
    # @param C is the covariance matrix
    def __init__(self, bunch_center = [0.42, 0, 0],
                       bunch_avg_beta = [0, 0.77*cst.clight*np.cos(0.035*np.pi/180), 
                                            0.77*cst.clight*np.sin(0.035*np.pi/180)],
                       xyz_sigmas =  0.01*np.array([0.5, 0.5, 0.5]),
                       beta_sigmas = cst.clight*np.array([0.001, 0.001, 0.001]),
                       start_proper_time = 0 ,
                       start_proper_time_sigma = 0):
        # mean
        #mu = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  !0!]
        mu = np.concatenate((bunch_center, bunch_avg_beta, [start_proper_time]), axis=None)
        #print(mu)
        # covariance matrix
        ps = xyz_sigmas
        bs = beta_sigmas
        st = start_proper_time_sigma
        C = np.array([[ ps[0], 0.0,  0,  0,  0, 0    , 0],
                      [ 0, ps[1],  0,  0,  0, 0      , 0],
                      [ 0,  0, ps[2],  0.0,  0, 0    , 0],
                      [ 0,  0, 0.0, bs[0],  0, 0     , 0],
                      [ 0,  0,  0,  0, bs[1], 0.0    , 0],
                      [ 0,  0,  0,  0,  0.0, bs[2]   , 0],
                      [ 0,  0,  0,  0,  0.0,  0      , st]])
        self.mu = mu
        self.cov = C
    
    # @param n is the number of particles
    # @return a (n, 7!!) matrix, each row an intitial condition
    def create(self, n):
        return np.random.multivariate_normal(self.mu, self.cov, n)
