import numpy as np
from numpy import cos,sin
import sympy as syp
from ai import cs # Coordinate transform library

from scipy.integrate import solve_ivp
import logging as logg
import argparse
import h5py
import os
import time

from mylib.physics import Constants,getgamma,getbeta
from mylib.settings import GlobalParameters
from mylib import particles_classes, utils, Distr_gauss


def sim_bunch(datadir,filename,N=1,pID = 1, initial_distr = None, max_lifetime = None, decay_time_pt = None):

    mylogg = logg.getLogger('mylog')

    if initial_distr is None:
        g = Distr_gauss.Gaussian()
        initial_bunch = g.create(N)
    else:
        initial_bunch = initial_distr
        N = len(initial_bunch)

    #rundir = utils.generate_run_folder('data')
    #filename = utils.generate_unique_filename(suffix='.h5')
    #h5f = h5py.File(f"{rundir}/{filename}", "w-")
    with h5py.File(f"{datadir}/{filename}", "w-") as h5f:
        for ii in range(N):
            startpt_tot = time.perf_counter()
            startproct_tot = time.process_time()
            mylogg.info(f"Start sim of: muon_p{pID}_{ii}, {ii}/{N}")
            sol, part_stat = sim_one_particle(#particle_id=0, 
                                particle_class=particles_classes.MuonEMClass(), 
                                y0=initial_bunch[ii],
                                Nvals=5000,
                                start_time = 0,
                                max_lifetime=max_lifetime,
                                safety_margin_factor = 1.2,
                                keep_every = 1,
                                decay_time_pt=decay_time_pt,
                                decay_percentile_to_ignore = GlobalParameters.decay_percentile_to_ignore,
                                )
                                
            isuccess = int(sol.success)
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/t", data=sol.t,compression="gzip", compression_opts=9)
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/y", data=sol.y,compression="gzip", compression_opts=9)
            print(sol.t_events)
            print(sol.y_events)
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/crossxp_t", data=sol.t_events[2],compression="gzip", compression_opts=9)
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/crossxp_y", data=sol.y_events[2],compression="gzip", compression_opts=9)
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/nfev", data=sol.nfev, dtype='i')
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/status", data=sol.status, dtype='i')
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/sol/success", data=isuccess, dtype='i')
            dset = h5f.create_dataset(f"sim/muon_p{pID}_{ii}/particle_status", data=part_stat, dtype='i')
            
            endpt_tot = time.perf_counter()
            endproct_tot = time.process_time()
            totpt = endpt_tot - startpt_tot
            totproct = endproct_tot - startproct_tot
            mylogg.info(f"muon_p{pID}_{ii} done, times: Sys: {totpt}; Proc: {totproct}")
    pass



def sim_one_particle(#particle_id=0, 
                     particle_class=particles_classes.MuonEMClass(), 
                     y0=None,
                     Nvals=5000, 
                     beta0=0.77,
                     start_time = 0,
                     max_lifetime=None,
                     safety_margin_factor = 1.2,
                     keep_every = 1,
                     decay_time_pt=None,
                     decay_percentile_to_ignore = GlobalParameters.decay_percentile_to_ignore,
                     ):
    mylogg = logg.getLogger('mylog')
    mylogg.debug("Solve equations of motion...")
    mc=particle_class
    c = mc.c
    ### Derivative lambda
    newt = lambda t, y : mc.derivatives(t,y)
    ###
    
    hl_rf=mc.hl_rf # Muon half-life, Comoving frame
    gamma = getgamma(np.array([beta0*c,0,0]), c=c)
    hl_lab = gamma*hl_rf # Muon half-life, laboratory SoR

    #pti = GlobalParameters.decay_percentile_to_ignore
    # _pt = measured in proper time
    # Max time to run the simulation (from inverse of cumulative of exponential distr)
    # APPROXIMATE distribution using initial gamma0, if gamma changes significantly the exponential will be off
    # Using 1.2 factor to be safe
    #safety_margin_factor = 1.2
    decay_cutoff_pt = -hl_rf*np.log(decay_percentile_to_ignore)
    max_time_to_run = gamma*decay_cutoff_pt*safety_margin_factor
    

    ### Set time span
    t_span = [start_time,max_time_to_run]
    #t_span = [0,hl_lab*0.03]

    if y0 is None:
        square_factor = 0.5
        phi_z = 0.035*np.pi/180     # [rad] Vertical angle in entrance. Positive: vel pointing up, negative down.
        theta = -0.008
        theta = square_factor*0.97*np.pi/180  # [rad] Horizontal angle in entrance.
                                    # Positive: particle vel pointing towards the outside (positive r'),
                                    # Negative: particle vel pointing towards the inside (negative r'),
        #phi_z = 1*np.pi/180
        mylogg.debug(f"phi_z = {phi_z}, sin(phi_z) = Pz/P = {np.sin(phi_z)}")
        mylogg.debug(f"Pz/P = {np.sin(phi_z)}")
        gyrorad_theory = GlobalParameters.gyrorad_theory # Gyroradius from theory
        y0 = [gyrorad_theory+0.030+0.014*square_factor,                 0,                   0,    # x, y, z
              beta0*c*cos(phi_z)*sin(theta), beta0*c*cos(phi_z)*cos(theta), beta0*c*sin(phi_z), 0]   # vx, vy, vz, proper_time_elapsed
    mylogg.debug(f"y0={y0}")
    
    max_stepsize = hl_lab/20000
    max_first_step = max_stepsize/5
    times = np.linspace(t_span[0], t_span[1], (Nvals+1)//keep_every)
    
    # Has the particle hit the top or bottom?
    acc_h = GlobalParameters.accelerator_height
    hit_tb = lambda t, y : np.abs(y[2])-acc_h/2 # Zero if z is at top/bottom
    hit_tb.terminal = True

    # When the particle crosses the X+ plane? When pos 'y' goes from neg to pos (!when counterclockwise!)
    cross_xp = lambda t, y : y[1]
    #cross_xp.terminal = False
    cross_xp.direction = 1

    # Has the particle decayed?
    if decay_time_pt is None:
        decay_time_pt = np.random.exponential(scale=hl_rf/np.log(2), size=None)
        if max_lifetime is None:
            # Default max particle lifetime is 3.5 times the half life (> ~99.7% of particles' lifes)
            decay_time_pt = hl_rf*3.5 if decay_time_pt>hl_rf*3.5 else decay_time_pt
        else:
            decay_time_pt = max_lifetime if decay_time_pt>max_lifetime else decay_time_pt
    
    # Zero if proper_time_elapsed has reached the time of decay, decay_time_pt
    has_decayed = lambda t, y : decay_time_pt - y[6] 
    has_decayed.terminal = True
    
    sol = solve_ivp(newt, t_span, y0, method='DOP853', # 'DOP853'
                                #t_eval=times,
                                first_step=max_first_step,
                                max_step=max_stepsize,
                                #dense_output=False, 
                                events=(hit_tb, has_decayed, cross_xp),
                                # vectorized=False, 
                                # args=None
                                rtol = 1e-10,
                               )
    #mylogg.debug(sol.t)
    #mylogg.debug(sol.y)
    mylogg.debug("Events times: ")
    mylogg.debug(sol.t_events)
    mylogg.debug(sol.message)

    evs = sol.t_events
    particle_status = -100
    t_hit = len(evs[0]) # times hit top/bottom event activated
    t_dec = len(evs[1]) # times decay event activated
    if sol.success == -1:
        mylogg.error("Integration algorithm failed!")
        particle_status = -10
        return sol, particle_status
    
    if t_hit == 1 and t_dec == 0:
        mylogg.debug("Particle hit top/bottom")
        particle_status = 10
    elif t_hit == 0 and t_dec == 1:
        mylogg.debug("Particle decayed")
        particle_status = 20
    elif t_hit == 0 and t_dec == 0 and sol.success == 0:
        mylogg.debug("Particle reached max lifespan, not tracked anymore")
        particle_status = 30
    else:
        mylogg.error("Nonsensical particle final state!")
        particle_status = -20
    
    return sol, particle_status


