import numpy as np
from numpy.linalg import norm
from mylib.physics import getgamma,getbeta
from mylib.settings import GlobalParameters
from ai import cs # Coordinate transform library

from .physics import Constants as cst
import logging as logg
from scipy.spatial.transform import Rotation
import scipy.interpolate
from .utils import ramp_down


# def sextupole_section(x, y, K, J=0):
#     mylogg = logg.getLogger('mylog_partclass')
#     Bx = J*(x**2 - y**2)+K*y*xcubic
#     By = -J*x*y + K*(x**2 - y**2)
#     mylogg.debug(f"|Bsext| = {np.sqrt(Bx**2+By**2)}")
#     return np.array([Bx,By])

class MuonEMClass:
    # c = Constants.clight
    # m = Constants.mmuon
    # q = -Constants.echarge
    c = cst.clight
    m_mev = 105.6583755e6 # Muon mass in GeV/c**2
    evc2_to_kg = 1.78266192e-36
    m = m_mev * evc2_to_kg #muon mass [Kg]
    charge_sign = -1
    q=charge_sign*1.602176634e-19
    hl_rf=2.1969811e-6 # [s] Muon half-life, Comoving frame
    gyrorad_theory = GlobalParameters.gyrorad_theory

    def __init__(self):
        xysec = GlobalParameters.Bz_pert_datapoints
        rmr0 = xysec[:,0]/100  # local x=r-r0, perturbator sector coordinates[m]
        Bz_p = xysec[:,1]/1e4*GlobalParameters.Bz_pert_scaleB # [Tesla]
        self.f_Bzsec = scipy.interpolate.interp1d(rmr0, Bz_p, kind=5, bounds_error=False, fill_value=0.)
        pass

    
    def B_perturbator(self, x, y, z, t):
        pi = np.pi

        # phi = 0 is the positive x axis, pos y = pi/2, neg x pi, neg y MINUS pi/2
        r, phi, z = cs.cart2cyl(x=x, y=y, z=z)
        p_ang_width = 40*pi/180. # 40 degrees
        p_end = -pi/2# |
        p_start=-pi/2-p_ang_width # /

        r0 = self.gyrorad_theory
        xl = r-r0
        x1 = r0-0.05
        x2 = r0+0.05
        if phi > p_start and phi < p_end and xl+r0 > x1 and xl+r0 < x2:
            #print('here')
            #Bz = 0.011*2/(x2-x1)*xl
            Bz = self.f_Bzsec(xl)
        else:
            Bz = 0.
        st = GlobalParameters.ramp_start_time
        et = GlobalParameters.ramp_end_time
        Bz = Bz*ramp_down(t=t, start_t = st, stop_t = et)
        return np.array([0,0,Bz])

    def E1_cart(self, x,y,z, t):
        x
        r, phi, z = cs.cart2cyl(x=x, y=y, z=z)
        return self.E1_cyl(r, phi, z, t)

    def B1_cart(self, x,y,z, t):
        r, phi, z = cs.cart2cyl(x=x, y=y, z=z)
        return self.B1_cyl(r, phi, z, t)+self.B_perturbator(x, y, z, t)

    def E1_cyl(self, r, phi, z, t):
        E = 0.64e6
        #E = 0
        Ev_cyl = np.array([E,0,0]) # In cyl coords
        M_cyl_to_cart = np.array([[np.cos(phi), -np.sin(phi), 0], 
                                  [np.sin(phi), np.cos(phi),  0],
                                  [0,        0,               1]])
        Ev_cart = M_cyl_to_cart @ Ev_cyl
        return Ev_cart

    def B1_cyl(self, r, phi, z, t):
        B = 1
        return np.array([0,0,B])

    def lorenz_force(self, p, v, t):
        mylogg = logg.getLogger('mylog_partclass')
        x,y,z = p
        r = np.sqrt(x**2+y**2)
        q  = self.q
        Ep = self.E1_cart(x,y,z, t)
        Bp = self.B1_cart(x,y,z, t)
        mylogg.debug(f"r: {r} [m]; Bp_z: {Bp[2]} [T]")
        return q*(Ep+np.cross(v,Bp))

    def relativistic_newton(self, F, v):
        """
        Relativistic newton second law.
        """
        c = self.c
        m = self.m
        gamma = getgamma(v,c)
        acc = 1.0/(m*gamma)*(F-np.dot(F,v)*v/c**2)
        return acc

    def derivatives(self, t, y):
        
        mylogg = logg.getLogger('mylog_partclass')
        # pte = proper_time_elapsed
        x,y,z, vx,vy,vz, pte = y
        pos = np.array([x,y,z])
        v = np.array([vx,vy,vz])

        c = self.c

        
        F = self.lorenz_force(pos, v, t)
        a = self.relativistic_newton(F, v)
        mylogg.debug(f"t: {t:6.3}; F: {norm(F):6.3} [N]; a: {norm(a):6.3} [m/s^2]")
        # ds/dt = 1/gamma(v(t))
        proper_time_increment = 1./getgamma(v,c)
        
        #der = np.append(v,a)
        der = np.concatenate((v, a, proper_time_increment), axis=None)
        return der