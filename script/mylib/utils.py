from datetime import datetime
import secrets
import logging as logg

import os
import numpy as np
from numpy import sin,cos

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as matpat
import scipy.optimize
import h5py
import collections.abc
import matplotlib.cm as cm
from matplotlib.collections import LineCollection

from .settings import *
from .physics import Constants,getgamma

# # from matplotlib import rc

# # rc('text',usetex=True)
# # rc('text.latex', preamble=r'\usepackage{color}')

# from matplotlib.backends.backend_pgf import FigureCanvasPgf
# matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)

# import matplotlib.pyplot as plt

# pgf_with_latex = {
#     "text.usetex": True,            # use LaTeX to write all text
#     "pgf.rcfonts": False,           # Ignore Matplotlibrc
#     "pgf.preamble": [
#         r'\usepackage{color}'     # xcolor for colours
#     ]
# }
# matplotlib.rcParams.update(pgf_with_latex)


#https://code.activestate.com/recipes/52308-the-simple-but-handy-collector-of-a-bunch-of-named/?in=user-97991
class Struct:
   """
   Class acting like a dynamic struct.
   Example use: 
      point = Struct(datum=y, squared=y*y, coord=x)
      if point.squared > threshold:
         point.isok = 1
   """
   def __init__(self, **kwds):
      self.__dict__.update(kwds)

def ramp_down(t, start_t=1, stop_t=2):
    y1 = 1
    y2 = 0
    x1 = start_t
    x2 = stop_t
    m = (y2-y1) / (x2-x1)
    if t <= start_t:
        return 1
    elif t > start_t and t <= stop_t:
        return m*(t-x1)+y1
    else:
        return 0
    pass

def generate_unique_filename(suffix):
    mylogg = logg.getLogger('myutil_log')
    dateTimeObj = datetime.now()
    time_string = dateTimeObj.strftime("%d%b%y-%H_%M_%S_%f")
    random_string = secrets.token_hex(3)
    filename = time_string+"-"+random_string+suffix
    mylogg.debug(filename)
    return filename

def generate_run_folder(basedir, runID = -1):
   if runID == -1:
      ii = 0
      # 'G' = generated number name
      rundir_path = f"./{basedir}/runG{ii}"
      while os.path.isdir(rundir_path):
         ii = ii+1
         rundir_path = f"./{basedir}/runG{ii}"
      if not os.path.exists(rundir_path):
         os.makedirs(rundir_path)
      else: 
         raise ValueError(f"Error: generated run folder '{rundir_path}' exist??")
      return rundir_path
   else:
      # 'M' = 'My' number name, should not exist
      rundir_path = f"./{basedir}/runM{runID}"
      os.makedirs(rundir_path,exist_ok=True)
      return rundir_path


def plotsol_3D(sol):
   #mpl.rcParams['legend.fontsize'] = 10
   mylogg = logg.getLogger('myutil_log')
   pft = GlobalParameters.plot_file_type
   pls = GlobalParameters.plot_show
   mylogg.info("Plot 3D")
   fig = plt.figure()
   ax = fig.gca(projection='3d')
   # ax.set_aspect('equal') # impossible in this pyplot version
   ax.set_proj_type('ortho') # 'ortho' or 'persp'
   xt,yt,zt = sol.y[0,:], sol.y[1,:], sol.y[2,:]
   mylogg.debug(yt)
   ax.plot(xt, yt, zt , marker='.', linestyle='',
      linewidth=2, markersize=1, label='Muon trajectory')
   ax.plot(xt[0:10], yt[0:10], zt[0:10], marker='.', linestyle='-',
      linewidth=1, markersize=6, label='Step size', c='r')
   
   ax.set_xlabel('X [m]')
   ax.set_ylabel('Y [m]')
   ax.set_zlabel('Z [m]')
   #ax.legend()
   ax.set(title='Muon 3D trajectory')
   fig.savefig(f"plots/XYZ_traj_plot.{pft}")
   if pls: plt.show()
   pass

def plotsol_2D_plus_time(sol):
   #mpl.rcParams['legend.fontsize'] = 10
   mylogg = logg.getLogger('myutil_log')
   pft = GlobalParameters.plot_file_type
   pls = GlobalParameters.plot_show
   mylogg.info("Plot XY and time")
   fig = plt.figure()
   ax = fig.gca(projection='3d')
   # ax.set_aspect('equal') # impossible in this pyplot version
   ax.set_proj_type('ortho') # 'ortho' or 'persp'
   t = sol.t
   xt,yt,zt = sol.y[0,:], sol.y[1,:], sol.y[2,:]
   mylogg.debug(yt)
   ax.plot(xt, yt, t , marker='.', linestyle='',
      linewidth=2, markersize=1, label='Muon trajectory')
   ax.plot(xt[0:10], yt[0:10], t[0:10], marker='.', linestyle='-',
      linewidth=1, markersize=6, label='Step size', c='r')
   
   ax.set_xlabel('X [m]')
   ax.set_ylabel('Y [m]')
   ax.set_zlabel('t [s]')
   #ax.legend()
   ax.set(title='Muon XY trajectory in time')
   fig.savefig(f"plots/XYt_plot.{pft}")
   if pls: plt.show()
   pass

def plotsol_2D(sol):
   mylogg = logg.getLogger('myutil_log')
   pft = GlobalParameters.plot_file_type
   pls = GlobalParameters.plot_show

   mylogg.info("Plot 2D")
   fig, ax = plt.subplots()
   ax.set_aspect('equal')
   start_orbit = sol.y[0:2,:100]
   end_orbit   = sol.y[0:2,-70:]
   ax.plot(sol.y[0,:], sol.y[1,:], marker='.', linestyle='',
      linewidth=2, markersize=1, color='grey', alpha=0.4)
   ax.plot(start_orbit[0,:], start_orbit[1,:], marker='.', linestyle='-', color='r',
      linewidth=2, markersize=1,)
   ax.plot(end_orbit[0,:], end_orbit[1,:], marker='.', linestyle='-', color='b',
      linewidth=2.5, markersize=1,)
   
   ### Plot perturbator and tube
   pert_plot = matpat.Wedge(center=(0., 0.),
                                        r=GlobalParameters.gyrorad_theory+0.05, # [m]
                                        theta1=-130, theta2=-90, 
                                        width=0.11, color='lightsteelblue', alpha=0.4) # [m]
   ax.add_artist(pert_plot)
   tube_plot = matpat.Wedge(center=(0., 0.),
                                        r=GlobalParameters.gyrorad_theory+0.05, # [m]
                                        theta1=-90, theta2=-130, 
                                        width=0.11, color='navajowhite', alpha=0.8) # [m]
   ax.add_artist(pert_plot)
   ax.add_artist(tube_plot)
   ###

   ### Find centers
   x0 = (0., 0., 0.425) # Default values for xc,yc,r
   xd = start_orbit[0,:]
   yd = start_orbit[1,:]
   opt_xyr_s = scipy.optimize.minimize(find_center_lossfun,
                                             x0,
                                             args=(start_orbit[0,:], start_orbit[1,:]),
                                             )
   opt_xyr_e = scipy.optimize.minimize(find_center_lossfun,
                                          x0,
                                          args=(end_orbit[0,:],end_orbit[1,:]),
                                          )
   ax.plot((opt_xyr_s.x[0],), (opt_xyr_s.x[1],), marker='o', linestyle='', color='r',
      linewidth=2, markersize=4,)
   ax.plot((opt_xyr_e.x[0],), (opt_xyr_e.x[1],), marker='o', linestyle='', color='b',
      linewidth=2, markersize=4,)
   #c1 = plt.Circle((opt_xyr_s.x[0], opt_xyr_s.x[1]), opt_xyr_s.x[2], color='orange', fill=True)
   #ax.add_artist(c1)
   ###

   ax.set(xlabel='X [m]', ylabel='Y [m]',
      title='Circular orbit in constant B')
   ax.grid()

   fig.savefig(f"plots/XY_traj_plot.{pft}")
   if pls: plt.show()
   pass

def plotsol_r_minus_r0(sol):
   mylogg = logg.getLogger('myutil_log')
   pft = GlobalParameters.plot_file_type
   pls = GlobalParameters.plot_show

   mylogg.info("Plot R-R_0")
   fig, ax = plt.subplots()
   #ax.set_aspect('equal')
   r0=GlobalParameters.gyrorad_theory
   r2 = np.sqrt(sol.y[0,:]**2 + sol.y[1,:]**2) - r0
   
   #print(r2)
   ax.plot(sol.t, r2, marker='.', linestyle='-',
      linewidth=2, markersize=1,)
   #ax.plot(r2)

   ax.set(xlabel='t [s]', ylabel='r-r0 [m]',
      title='Orbit alignment to r0')
   ax.grid()

   fig.savefig(f"plots/RmR0.{pft}")
   if pls: plt.show()

def find_center_lossfun(xyr, xcoords, ycoords):
   """
   Loss function for finding the center of a set of points approximately along a circumference.

   Variables for the fitter:
   xyr --  3-array containing
      0: xc -- the x coordinate of the center, to be fit
      1: yc -- the y coordinate of the center, to be fit
      2: r  -- radius from the points to the center, to be fit.
             (Important bc imposes the center to be equidistant from all the points).
   
   Parameters:
   xcoords -- x coordinates of the data along the circumference
   ycoords -- y coordinates of the data along the circumference
   """
   xc = xyr[0]
   yc = xyr[1]
   r  = xyr[2]
   return sum((np.sqrt((xcoords-xc)**2 + (ycoords-yc)**2)-r)**2)

def make_square_initcond(beta0=0.77, every = 1):
   mylogg = logg.getLogger('myutil_log')
   c = Constants.clight
   ## r=30/44, r'= -4/18
   _MILLI = 1e-3
   square_factor = 0.5
   #phi_z = 0.035*np.pi/180     # [rad] Vertical angle in entrance. Positive: vel pointing up, negative down.
   phi_z = 0.
   # theta = square_factor*0.97*np.pi/180  # [rad] Horizontal angle in entrance.
   #                            # Positive: particle vel pointing towards the outside (positive r'),
   #                            # Negative: particle vel pointing towards the inside (negative r'),
   
   gyrorad_theory = GlobalParameters.gyrorad_theory
   min_r = 30 # [mm]
   max_r = 44 # [mm]
   min_rp = -4
   max_rp = 18
   nr = (max_r-min_r + 1) // every
   nrp = (max_rp-min_rp + 1) // every
   r_axis =  _MILLI*np.linspace(min_r, max_r, nr)
   rp_axis = _MILLI*np.linspace(min_rp, max_rp, nrp)

   rr, pp = np.meshgrid(r_axis, rp_axis, sparse=False, indexing='xy')
   
   # return a (n, 7!!) matrix, each row an intitial condition
   init_conds = np.zeros((nr*nrp,7), dtype=float, order='C')
   for yi in range(nrp): 
      for xj in range(nr):
         
         mylogg.info(f"r = {rr[yi,xj]}, r' = {pp[yi,xj]}")
         #print(rr[yi,xj],pp[yi,xj])   # con 'xy', itera prima in orizzontale la griglia, (sinistra->destra 
                                       # poi giu->su)
         rpos  = rr[yi,xj]
         theta = pp[yi,xj]# r' in millirad
         y0 = [gyrorad_theory+rpos,                 0,                   0,    # x, y, z
               beta0*c*cos(phi_z)*sin(theta), beta0*c*cos(phi_z)*cos(theta), beta0*c*sin(phi_z), 0]   # vx, vy, vz, proper_time_elapsed

         init_conds[yi*nr + xj,0:7]=y0
   return init_conds

def plot_file(run_id, particle_id=1):
   datadir = f"./data/run{run_id}/"
   filename = os.listdir(datadir)[0]
   datafile = datadir + filename
   with h5py.File(datafile, "r") as h5f:
         group = f"/sim/muon_p1_{particle_id}/sol/"
         s_t = h5f.get(group + 't')
         s_y = h5f.get(group + 'y')
         sol = Struct(y=s_y, t=s_t)

         plotsol_2D(sol)
         plotsol_3D(sol)
         plotsol_2D_plus_time(sol)


def plotsol_rphase(run_id, n_particles=1, decay=False):
   #mpl.rcParams['legend.fontsize'] = 10
   mylogg = logg.getLogger('myutil_log')
   pft = GlobalParameters.plot_file_type
   pls = GlobalParameters.plot_show
   mylogg.info("Plot r phase space")
   fig, ax = plt.subplots()
   _MILLI = 1e-3

   datadir = f"./data/run{run_id}/"
   filename = os.listdir(datadir)[0]
   datafile = datadir + filename
   with h5py.File(datafile, "r") as h5f:
      for ii in range(n_particles):
         group = f"/sim/muon_p1_{ii}/sol/"
         s_t = h5f.get(group + 't')
         s_y = h5f.get(group + 'y')
         s_xp_t = h5f.get(group + 'crossxp_t')
         s_xp_y = h5f.get(group + 'crossxp_y')

         sol = Struct(y=s_y, t=s_t)
         xt,yt,zt = sol.y[0,:], sol.y[1,:], sol.y[2,:]
         vxt,vyt,vzt = sol.y[3,:], sol.y[4,:], sol.y[5,:]

         ti = 0
         ri = sol.y[0:3,ti]
         vi = sol.y[3:6,ti]
         ri_hat = ri / np.linalg.norm(ri)

         vir = np.arcsin(np.dot(vi,ri_hat)/np.linalg.norm(vi)) / _MILLI
         ax.plot((ri[0],), (vir,), marker='o', linestyle='', color='r',
            linewidth=2, markersize=4,)


         # Plot "trajectories"
         if not decay:
            nome = "XZ_section_plot_magnet"
            rf_hat = np.zeros(shape=(len(s_xp_y),3))
            vfr = np.zeros(shape=(len(s_xp_y)))
            for i in range(len(s_xp_y)):
               rf = s_xp_y[i,0:3]
               vf = s_xp_y[i,3:6]
               rf_hat[i,:] = rf / np.linalg.norm(rf)
               vfr[i] = np.arcsin(np.dot(vf,rf_hat[i,:])/np.linalg.norm(vf)) / _MILLI
               #vfr[i] = np.dot(vf,rf_hat[i,:])
            
            
            pert_factor = np.array([ramp_down(s_xp_t[i],
                           start_t=GlobalParameters.ramp_start_time, 
                           stop_t=GlobalParameters.ramp_end_time) for i in range(len(s_xp_t))])
            cmap_color = cm.viridis
            print(f"N of turns for particle {ii}: {len(s_xp_t)}")
            #print(pert_factor)
            # ax.plot(s_xp_y[:,0], vfr, marker='.', linestyle='-', color='gray',
            # linewidth=0.5, markersize=2, alpha=0.4)
            # 
            ax.scatter(s_xp_y[:,0], vfr, c=pert_factor, cmap=cmap_color, s=1)

            points = np.array([s_xp_y[:,0], vfr]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            lc = LineCollection(segments, cmap=cmap_color)
            lc.set_array(pert_factor)
            lc.set_linewidth(2)
            line = ax.add_collection(lc)
         else:
            nome = "XZ_section_plot_decay"
            rf_hat = np.zeros(shape=(len(s_xp_y),3))
            vfr = np.zeros(shape=(len(s_xp_y)))
            for i in range(len(s_xp_y)):
               rf = s_xp_y[i,0:3]
               vf = s_xp_y[i,3:6]
               rf_hat[i,:] = rf / np.linalg.norm(rf)
               vfr[i] = np.arcsin(np.dot(vf,rf_hat[i,:])/np.linalg.norm(vf)) / _MILLI
               #vfr[i] = np.dot(vf,rf_hat[i,:])
            
            beta0 = 0.77
            hl_rf = Constants.muon_hl_rf
            c = Constants.clight
            gamma = getgamma(np.array([beta0*c,0,0]), c=c)
            hl_lab = gamma*hl_rf
            betaexp = hl_lab/np.log(2)
            #expon = lambda t: 1.0/betaexp * np.exp(-t/betaexp)
            expon = lambda t: np.exp(-t/betaexp)
            decay_factor = np.array([expon(s_xp_t[i]) for i in range(len(s_xp_t))])
            decay_factor[-1] = 0
            cmap_color = cm.coolwarm
            print(f"N of turns for particle {ii}: {len(s_xp_t)}")
            # ax.plot(s_xp_y[:,0], vfr, marker='.', linestyle='-', color='gray',
            # linewidth=0.5, markersize=2, alpha=0.4)
            # 
            ax.scatter(s_xp_y[:,0], vfr, c=decay_factor, cmap=cmap_color, s=1)

            points = np.array([s_xp_y[:,0], vfr]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            lc = LineCollection(segments, cmap=cmap_color)
            lc.set_array(decay_factor)
            lc.set_linewidth(2)
            line = ax.add_collection(lc)

            #xy_hl = np.min(np.abs(array - hl_lab))

         # tf = -1
         # rf = sol.y[0:3,tf]
         # vf = sol.y[3:6,tf]

         # Plot half life point
         


         rf = s_xp_y[-1,0:3]
         vf = s_xp_y[-1,3:6]
         rf_hat = rf / np.linalg.norm(rf)

         vfr = np.arcsin(np.dot(vf,rf_hat) / np.linalg.norm(vf)) / _MILLI
         ax.plot((rf[0],), (vfr,), marker='o', linestyle='', color='b',
            linewidth=2, markersize=4,)


   # ax.set_aspect('equal') # impossible in this pyplot version
   # xt,yt,zt = sol.y[0,:], sol.y[1,:], sol.y[2,:]
   # mylogg.debug(yt)
   # ax.plot(xt, yt, zt , marker='.', linestyle='',
   #    linewidth=2, markersize=1, label='Muon trajectory')
   # ax.plot(xt[0:10], yt[0:10], zt[0:10], marker='.', linestyle='-',
   #    linewidth=1, markersize=6, label='Step size', c='r')
   
   # Draw stable orbit and r'=0 axis
   # cb = fig.colorbar(line, #norm=matplotlib.colors.Normalize(vmin=0, vmax=1, clip=False),
   #        ticks=[0., 0.5, 1.])
   #plt.colorbar(norm=matplotlib.colors.Normalize(vmin=0, vmax=1, clip=False), cmap=cmap_color)
   #plt.clim(-4,4) 0.36722243 36.72%
   if decay:
      cb = fig.colorbar(line, ticks=[0., 0.36722243, 0.5, 1.])
      cb.ax.set_yticklabels(['0%', r'37%', '50%', '100%'])
      cb.ax.set_ylabel(r'% of particles left') #, rotation=270)
   else:
      cb = fig.colorbar(line, ticks=[0., 0.5, 1.])
      cb.ax.set_yticklabels(['0%', '50%', '100%'])
      cb.ax.set_ylabel(r'% of max. perturbator strength')
   plt.axvline(x=GlobalParameters.gyrorad_theory, color='g', linestyle='--')
   plt.axhline(y=0, color='g', linestyle='-')

   ax.set_xlabel(r"X [m]")
   ax.set_ylabel(r"X' $[\mu rad]$")
   #ax.legend()
   ax.set(title=r'Radial phase space')
   #ax.set(title=r'Radial phase space, in the $\overline{X_+ Y} plane$')
   fig.savefig(f"plots/{nome}_{n_particles}.{pft}")
   if pls: plt.show()
   pass