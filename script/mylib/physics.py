
from scipy.constants import codata # see: http://docs.scipy.org/doc/scipy/reference/constants.html

import numpy as np
import logging as logg
mylogg = logg.getLogger('mylog')

class Constants:
    clight     = codata.value('speed of light in vacuum')                   # m / s
    echarge    = codata.value('elementary charge')                          # C
    pmass      = codata.value('proton mass energy equivalent in MeV')       # MeV / c^2
    emass      = codata.value('electron mass energy equivalent in MeV')     # MeV / c^2
    mmuon       = codata.value('muon mass energy equivalent in MeV')         # MeV / c^2
    mdeuteron   = codata.value('deuteron mass energy equivalent in MeV')     # MeV / c^2
    mtau        = codata.value('tau mass energy equivalent in MeV')          # MeV / c^2
    mtriton     = codata.value('triton mass energy equivalent in MeV')       # MeV / c^2
    #malpha      = codata.value('alpha mass energy equivalent in MeV')       # MeV / c^2
    epsilon0   = codata.value('electric constant')                          # F / m
    muon_hl_rf=2.1969811e-6 # [s] Muon half-life, Comoving frame
    

def getgamma(v, c=Constants.clight):

    g=1./np.sqrt( 1. - v.dot(v) / c ** 2 )
    #mylogg.debug(f"gamma value: g({v})={g}")
    return g

def getbeta(v, c=1):
    return v/c